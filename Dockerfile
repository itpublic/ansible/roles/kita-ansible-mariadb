FROM mariadb:10.4
# https://hub.docker.com/_/mysql

# introduce a way of running scripts when the container starts regardless
# https://gist.github.com/JonathonReinhart/d117eea3b149261ced809e6a5d027cf4

COPY container/custom-entrypoint.sh /
RUN chmod +x /custom-entrypoint.sh
ENTRYPOINT ["/custom-entrypoint.sh"]
CMD ["mysqld"]