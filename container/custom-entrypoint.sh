#!/bin/bash
#
# 19.12.2023 edpck based on other code allow root user and introduced /custom-init.d
# Alternate entrypoint script to run additional initialization on every startup
#
# References:
#   https://github.com/docker-library/mariadb/issues/284#issuecomment-575349157
#   https://github.com/docker-library/mysql/blob/aaecc90a37/5.6/docker-entrypoint.sh

set -eo pipefail

echo "args: $@"
echo "sourcing docker-entrypoint.sh"
source "$(which docker-entrypoint.sh)"

mysql_note "Custom entrypoint script for MySQL Server ${MYSQL_VERSION} started."

# if /custom-init.d exists process init files (*.sh)
CUSTOM_DIR="/custom-init.d"
if test -n "$(shopt -s nullglob; echo $CUSTOM_DIR/*)"; then
    for f in $CUSTOM_DIR/* ; do
        case "$f" in
            *.sh)
                # https://github.com/docker-library/postgres/issues/450#issuecomment-393167936
                # https://github.com/docker-library/postgres/pull/452
                if [ -x "$f" ]; then
                    mysql_note "$0: running $f"
                    "$f"
                else
                    mysql_note "$0: sourcing $f"
                    # ShellCheck can't follow non-constant source. Use a directive to specify location.
                    # shellcheck disable=SC1090
                    . "$f"
                fi
                ;;
        esac
    done
fi

# call the original main function
_main "$@"