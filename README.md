# Kita

_BETA: [Ansible role](https://docs.ansible.com/ansible/latest/user_guide/playbooks_reuse_roles.html) for running container in "Kontainerdrift for IT-Avdelingen (KITA)". This is a contribution from other parts of the ITA_

This repo is addon toolbox for [KITA](https://wiki.app.uib.no/itwiki/index.php/Konteinerdrift_ved_ITA_(KITA)) using [Ansible](https://docs.ansible.com/ansible/latest/getting_started/index.html). 

It adds:
* [MariaDB](https://hub.docker.com/_/mysql) as mysql database

[[_TOC_]]

## Prerequisites
* **Access**: This documentation assumes you have access to this repo over SSH. Most members of ITA has access. See [this article](https://itgit.app.uib.no/help/user/ssh.md) for info about setting up your SSH key.

* **Software**: You will need ansible and ansible-galaxy on your local machine.

* **Hardware**: Sudo access on a server set up using the UiB spesific _kita_ puppet role.

## Getting started
You will need to include this ansible role into your playbook, but this role will not be contained in your repo. Check out this [project template](https://itgit.app.uib.no/it/ft/pf/ansible/project-templates/ansible-project-template-kita) or [project in actuall use](https://itgit.app.uib.no/it/sv/ark/ansible-kita-ark).

### MariaDB docker container
https://hub.docker.com/_/mysql

### Requirements

To start using the MariaDB role, add this to your [requirements.yml file](./requirements.yml):

```yaml
---
collections:
  - name: community.docker
    version: 3.4.8
    type: galaxy

roles:
  - src: git+git@itgit.app.uib.no:it/ft/pf/ansible/roles/kita-ansible.git
    version: "1.2.0" # Set the desired version in quotes, delete for latest unstable.
  - src: git+git@git.app.uib.no:itpublic/ansible/roles/kita-ansible-mariadb.git
    version: "1.3.17" # This is a string, mapping to the git tag in the repo
```

...and run this command from the root of the repo:
```sh
ansible-galaxy install -fr ./requirements.yml
```

### Variables
See [kita-ansible](https://itgit.app.uib.no/it/ft/pf/ansible/roles/kita-ansible#variables) for needed variables and other configuration that may be need.

For MariaDB you need to define variables in:
* `group_vars/vault_[test|prod].yml` (environment spesific)
* `group_vars/vault.yml` (encrypted for all environments)

Example of how to add the MariaDB role to your own roles/myapplication/tasks/main.yml

```yaml
- name: use kita-ansible-mariadb role to make sure MariaDB is present
  include_role:
    name: kita-ansible-mariadb
  vars:
    mariadb_env: test
    mariadb_root_password: "{{ vault_mariadb_root_password }}"
    mariadb_backup:
      - notification_email: "{{ kita_ark_notification_email }}"
```

You also need to add password to [ansible-vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) in an encrypted file called `group_vars/vault_[test|prod].yml`:

```yaml
vault_mariadb_root_password: randompassword
```

### Execution
Lastly, add the role in top of your playbook (`site.yml`):

```
---
- name: "Setup Konteinerdrift for ITA – Ansible (KITA-A)"
  hosts: kita
  roles:
    - kita-ansible
  tags:
    - kita

- name: "Setup myapplication"
  hosts: myapplication
  vars_files:
    - secrets/myapplication-{{env}}.yml
    - group_vars/vault_{{env}}.yml
  roles:
    - myapplication
  tags:
    - myapplication
```

...and execute (dryrun, remove _--check_ to actually run):
```
ansible-playbook site.yaml --inventory test.ini --ask-pass --become --ask-become-pass --ask-vault-password --diff --check
```

### Use an existing MariaDB
If you would like to use an existing mariaDB and the version is compatible with the docker image - do the following on your docker host server:

```
docker stop kita-ansible-mariadb
rm -rf /local/docker/kita-ansible-mariadb/data/*
cp -av existing-var-lib-mysql-folder/* /local/docker/kita-ansible-mariadb/data/
docker start kita-ansible-mariadb
```

### Php-example how to connect to the MariaDB
The MariaDB container port 3306 is exposed on the docker-host (ie: t1arkd02.uib.no) port 3306. That means that a php program in another container on the same docker-host or another can access it by using the hostname or a DNS alias. Make sure docker-host:local firewall allows access to port 3306 from other host(s).

```php
  try {
    // tips: use a DNS alias like myapp.mbtest.uib.no instead of the docker-hostname: t1arkd02.uib.no -
    // makes it easier to move the container to another docker-host without needing to change the php code.
    $db = new PDO("mysql:host=myapp.mbtest.uib.no;dbname=mydbname","username","password");
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
  } catch (PDOException $e) {
    die("Error: ".$e->getMessage());
  }
```

### Disable MariaDB Backup

If you decide not to have backup of MariaDB (in /local/docker/kita-ansible-mariadb/data-backups), just remove 
"mariadb_backup" from the yaml:

```yaml
- name: use kita-ansible-mariadb role to make sure MariaDB is present
  include_role:
    name: kita-ansible-mariadb
  vars:
    mariadb_env: prod
    mariadb_root_password: "{{ vault_mariadb_root_password }}"
```

### Edit this repo

Check out gitrepo:

```
git clone git@git.app.uib.no:itpublic/dockerimages/ansible/kita-ansible-mariadb.git

# edit files, commit and add new tag (first find oldest one)

git tag
git tag -a 1.3.17 -m "1.3.17"; git push origin --tags
```