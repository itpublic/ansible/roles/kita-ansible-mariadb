#!/bin/bash

# gpp a bash script that is run inside a container to check that /etc/hosts has gateway ip mapped to the docker host name and adds if not present
# ...

# Get the gateway IP of the container
gateway_ip=$(ip route | awk '/default/ { print $3 }')
# Docker hostname comes from extra env when docker is starting the container
hostname=$DOCKER_HOSTNAME

# Check if gateway IP is mapped to docker host name
if ! grep -q "$gateway_ip" /etc/hosts; then
    # Add the mapping if not present
    echo "# local-update-hosts.sh adding entry" >> /etc/hosts
    echo "$gateway_ip      $hostname" >> /etc/hosts
fi

