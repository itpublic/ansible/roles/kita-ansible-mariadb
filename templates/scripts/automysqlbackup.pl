#!/usr/bin/perl
#
# Do not run all backups at the same time.
# Select a random minute in cron
# Random number: https://www.random.org/integers/?num=1&min=1&max=59&col=5&base=10&format=html&rnd=new 
#

if (-f "/usr/bin/mysqldump" ) {
  #mysqldump 
} else {
  print "mysqldump do not exist!";
  exit 10;
}

$targetDir="/var/backups";
unless( -d $targetDir){
  system("mkdir -p $targetDir");
}
chdir($targetDir);
#only backup database that include string in @ARGV[0]
if (@ARGV[0]){
  $mustInclude=$ARGV[0]
}

$dbuser="root";
$mysql_pass="/root/mysql";
if (-f $mysql_pass) {
  #mysqldump use this file for password.
} else {
  print "File does not exist!";
  exit 1;
}

# databases to skip
@except = ("performance_schema","information_schema","munin");

# find mysql directories (usually DBs)
if ( $mustInclude ) {
  @files=`find /var/lib/mysql/* -type d -iname '*$mustInclude*'`;
} else {
  @files=`find /var/lib/mysql/* -type d`;
}
system("logger -t  mysqlbackup 'Target dir: $targetDir'");
foreach $file (@files){
  $file =~ s/.*\/([^\/]+)\n$/$1/;
  # dump this database if not in except list
  unless(grep(/^$file$/,@except)){
    #Test if transaction can be used
    #echo "SELECT count(*) FROM INFORMATION_SCHEMA.TABLES where engine='MyISAM' and TABLE_SCHEMA='mysql';" |mysql
    $countMyISAM=`echo "SELECT count(*) FROM INFORMATION_SCHEMA.TABLES where engine='MyISAM' and TABLE_SCHEMA='${file}';" |mysql|grep -v ^count`;
    if ( $countMyISAM == 0 ) {
      $opt=' --single-transaction ';
      system("logger -t  mysqlbackup Database:${file} Using transaction");
    } else {
      $opt=' --lock-tables ';
      $countMyISAM=$countMyISAM + 0 ;
      system("logger -t  mysqlbackup Database:${file} ${countMyISAM} MyISAM found locking tables");
    }
    system("mysqldump  ${opt} -u $dbuser --opt $file > mbackup-daynr_`date +%u`.$file.sql.tmp && logger -t  mysqlbackup Database:${file} backup ok || logger -t mysqlbackup Database:${file} backup ERROR ");
    #Gzip the files (--rsyncable = dedupe friendly)
    system("nice gzip --force --rsyncable mbackup-daynr_`date +%u`.$file.sql.tmp");
    system("mv mbackup-daynr_`date +%u`.$file.sql.tmp.gz  mbackup-daynr_`date +%u`.$file.sql.gz");
    # taking a pause, to let the waiting connections do their job.
    unless(@ARGV[1] eq 'nosleep') {
      system("sleep 10");
    }
  }
}

# remove old backup-files
system("find $targetDir -type f  -iname '*sql.gz'  -ctime +7 -exec rm -fv {} \\; | logger -t mysqlbackup ");
system("find $targetDir -type f  -iname '*sql.tmp' -ctime +2 -exec rm -fv {} \\; | logger -t mysqlbackup ");

#Next line creates problems if the authentication fails, it will then hang on a new password prompt, thereby no completing the script.
#system ( "if [ -x /usr/bin/dsmc ] ;then dsmc incr /backup &>/var/log/dscm-mbackup.log ; fi");
#
system("logger -t  mysqlbackup 'Backup done.'");
